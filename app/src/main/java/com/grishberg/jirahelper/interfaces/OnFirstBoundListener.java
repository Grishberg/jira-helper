package com.grishberg.jirahelper.interfaces;

/**
 * Created by g on 10.03.16.
 */
public interface OnFirstBoundListener {
    void onFirstBound();
}
