package com.grishberg.jirahelper.interfaces;

import com.grishberg.jirahelper.framework.BaseRestRequest;
import com.grishberg.jirahelper.framework.RestCallback;

/**
 * Created by g on 10.03.16.
 */
public interface FragmentInteractionListener {
    void onTaskSelected(int taskId);

    void onUserSelected(int userId);

    boolean onSendRequest(BaseRestRequest request, RestCallback callback);
}
