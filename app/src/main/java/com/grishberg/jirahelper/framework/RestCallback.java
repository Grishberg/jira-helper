package com.grishberg.jirahelper.framework;

import java.io.Serializable;

/**
 * Created by grishberg on 04.02.16.
 */
public interface RestCallback<T> {
    void onResponse(T response);
    void onFail(int code, String message);
}
