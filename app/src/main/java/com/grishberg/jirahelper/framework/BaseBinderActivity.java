package com.grishberg.jirahelper.framework;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by g on 07.10.15.
 */
public abstract class BaseBinderActivity<T extends BaseBinderService> extends AppCompatActivity {
    private static final String TAG = BaseBinderActivity.class.getSimpleName();
    private boolean mIsBound;
    private Intent mIntent;
    private boolean mIsBroadcastRegistered;
    private IntentFilter mLocalBroadcast;
    protected T mService;
    private boolean mIsFirstBind = true;
    private ConcurrentHashMap<Integer, RestCallback> restCallbacks;
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected: " + BaseBinderActivity.this.getClass().getSimpleName());
            mIsBound = true;
            if (isFinishing()) {
                Log.d(TAG, "onServiceConnected: is finishingm need to unbind");
                unregisterBroadcast();
                unbindFromService();
                return;
            }
            mService = (T) ((BaseBinderService.ApiServiceBinder) service).getService();
            if (mIsFirstBind) {
                mIsFirstBind = false;
                onFirstBound();
            }
            onBound();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mIsBound = false;
        }
    };

    protected void onFirstBound() {

    }

    protected abstract Intent getServiceIntent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIntent = getServiceIntent();
        mLocalBroadcast = new IntentFilter(Const.SERVICE_ACTION_TASK_DONE);
        mLocalBroadcast.addAction(Const.SERVICE_ACTION_TASK_FAIL);
        mIsFirstBind = true;
        restCallbacks = new ConcurrentHashMap<>();
        registerBroadcast();
        startService(getServiceIntent());
        bindToService();
    }

    @Override
    public void onPause() {
        super.onPause();
        unbindFromService();
        unregisterBroadcast();
    }

    @Override
    public void onResume() {
        super.onResume();
        bindToService();
        registerBroadcast();
    }

    /*
        @Override
        public void finish() {
            super.finish();
            unregisterBroadcast();
            unbindFromService();
        }
    */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcast();
        unbindFromService();
    }

    private void registerBroadcast() {
        if (!mIsBroadcastRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mMessageReceiver, mLocalBroadcast);
            mIsBroadcastRegistered = true;
        }
    }

    private void unregisterBroadcast() {
        if (mIsBroadcastRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(
                    mMessageReceiver);
            mIsBroadcastRegistered = false;
        }
    }

    private void bindToService() {
        if (!mIsBound) {
            bindService(mIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private void unbindFromService() {
        Log.d(TAG, String.format("unbindFromService: isBound = %b ", mIsBound) + this.getClass().getSimpleName());
        if (mIsBound) {
            unbindService(mServiceConnection);
            mIsBound = false;
            mService = null;
        }
    }

    protected void onBound() {
    }

    protected void onTaskDone(String tag, int taskId, Serializable response) {
        Log.d(TAG, String.format("onTaskDone: tag = %s, taskId = %d ", tag, taskId) +
                this.getClass().getSimpleName());
        RestCallback callback = restCallbacks.get(taskId);
        if (callback != null) {
            callback.onResponse(response);
            restCallbacks.remove(taskId);
        }
    }

    protected void onTaskFail(String tag, int taskId, int code) {
        RestCallback callback = restCallbacks.get(taskId);
        if (callback != null) {
            callback.onFail(code, null);
            restCallbacks.remove(taskId);
        }
    }

    protected boolean sendRequest(BaseRestRequest request, RestCallback callback) {
        if (mIsBound) {
            int id = mService.sendRequest(request);
            restCallbacks.put(id, callback);
            return true;
        }
        return false;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override

        public void onReceive(Context context, Intent intent) {
            String tag;
            int id;
            int code;
            Serializable data;
            switch (intent.getAction()) {
                case Const.SERVICE_ACTION_TASK_DONE:
                    tag = intent.getStringExtra(Const.EXTRA_TASK_TAG);
                    id = intent.getIntExtra(Const.EXTRA_TASK_ID, -1);
                    code = intent.getIntExtra(Const.EXTRA_TASK_CODE, -1);
                    data = intent.getSerializableExtra(Const.EXTRA_TASK_SERIALIZABLE);
                    onTaskDone(tag, id, data);
                    break;
                case Const.SERVICE_ACTION_TASK_FAIL:
                    tag = intent.getStringExtra(Const.EXTRA_TASK_TAG);
                    id = intent.getIntExtra(Const.EXTRA_TASK_ID, -1);
                    code = intent.getIntExtra(Const.EXTRA_TASK_CODE, -1);
                    onTaskFail(tag, id, code);
                    break;
            }
        }
    };

    /**
     * send messages to service
     *
     * @param code
     */
    private void sendMessageToService(int code) {
        Intent intent = new Intent(Const.ACTIVITY_ACTION);
        intent.putExtra(Const.EXTRA_TASK_CODE, code);
        LocalBroadcastManager.getInstance(this).
                sendBroadcast(intent);
    }
}
