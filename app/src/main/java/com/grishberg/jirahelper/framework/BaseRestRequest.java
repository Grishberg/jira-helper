package com.grishberg.jirahelper.framework;

import android.content.Context;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by grishberg on 08.03.16.
 */
public abstract class BaseRestRequest<T,S,A> {
    private int id;
    private String authToken;
    private Context context;
    public void setRequestId(int id){
        this.id = id;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAccessToken() {
        return authToken;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public int getRequestId() {
        return id;
    }

    public abstract Response onRequest(S service) throws IOException;
    public A onSuccess(T response){
        return null;
    }
    public void onFail(int errorCode){

    }
    public void onDone(T response){

    }
}
