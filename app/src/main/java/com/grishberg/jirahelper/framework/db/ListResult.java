package com.grishberg.jirahelper.framework.db;

/**
 * Created by g on 05.02.16.
 */
public interface ListResult<T> {
    T getItem(int index);
    int getCount();
}
