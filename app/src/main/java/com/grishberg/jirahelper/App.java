package com.grishberg.jirahelper;

import android.app.Application;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by grishberg on 07.03.16.
 */
public class App extends Application {
    private static final String TAG = App.class.getSimpleName();
    private static boolean sDbInitiated = false;
    @Override
    public void onCreate() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
        super.onCreate();
        Log.d(TAG, String.format("onCreate: v.%s:%d", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));
        initRealm();
    }

    private void initRealm(){

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void[] params) {
                RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext())
                        .deleteRealmIfMigrationNeeded()
                        .build();
                Realm.setDefaultConfiguration(config);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //TODO: send broadcast
                sDbInitiated = true;
            }
        }.execute();
    }

    public static boolean isDbInitiated() {
        return sDbInitiated;
    }
}
