package com.grishberg.jirahelper.data.services;

import android.util.Log;

import com.grishberg.jirahelper.data.rest.RestWorker;
import com.grishberg.jirahelper.framework.BaseBinderService;
import com.grishberg.jirahelper.framework.BaseRestRequest;

import java.io.Serializable;

/**
 * Created by grishberg on 08.03.16.
 */
public class RestService extends BaseBinderService implements RestWorker.ResponseListener {
    private static final String TAG = RestService.class.getSimpleName();
    private RestWorker restWorker;
    private int requestId;

    public RestService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
        restWorker = new RestWorker(getApplicationContext(), this);
    }

    @Override
    public int sendRequest(BaseRestRequest request) {
        requestId++;
        if (requestId > 0xFFFF) {
            requestId = 0;
        }
        request.setRequestId(requestId);
        restWorker.sendRequest(request);
        return requestId;
    }

    @Override
    public void onSuccess(Object o, int requestId) {
        Log.d(TAG, String.format("onSuccess: requestId = %d", requestId));
        sendMessage((Serializable) o, requestId);
    }

    @Override
    public void onFail(int code, String message, int requestId) {
        sendMessage(code, message, requestId);
    }
}
