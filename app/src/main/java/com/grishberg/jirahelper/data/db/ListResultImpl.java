package com.grishberg.jirahelper.data.db;

import com.grishberg.jirahelper.framework.db.ListResult;

import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by grishberg on 08.03.16.
 */
public class ListResultImpl <T extends RealmObject> implements ListResult<T> {
    private RealmResults<T> realmResults;
    public ListResultImpl(RealmResults<T> realmResults){
        this.realmResults = realmResults;
    }

    @Override
    public T getItem(int index) {
        return realmResults.get(index);
    }

    @Override
    public int getCount() {
        return realmResults.size();
    }
}
