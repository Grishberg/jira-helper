package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;
import com.grishberg.jirahelper.data.rest.RestConst;

/**
 * Created by g on 10.03.16.
 */
public class UsersResponse {
    @SerializedName(RestConst.Fields.META)
    public Meta meta;
    @SerializedName(RestConst.Fields.DATA)
    public UsersData data;
    @SerializedName(RestConst.Fields.PAGINATION)
    public Pagination pagination;
}
