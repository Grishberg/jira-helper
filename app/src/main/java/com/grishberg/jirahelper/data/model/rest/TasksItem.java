package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

public class TasksItem {
	@SerializedName(RestConst.Fields.ID)
	private int id;
	@SerializedName(RestConst.Fields.KEY)
    private String key;
	@SerializedName(RestConst.Fields.TITLE)
    private String title;
	@SerializedName(RestConst.Fields.PROJECT_ID)
    private int projectId;
	@SerializedName(RestConst.Fields.PLANNED)
    private int planned;
	@SerializedName(RestConst.Fields.FACT)
    private int fact;
	@SerializedName(RestConst.Fields.SPENT)
    private int spent;
	@SerializedName(RestConst.Fields.CUSTOMER)
    private Customer customer;
	@SerializedName(RestConst.Fields.ASSIGNEE)
    private Assignee assignee;
	@SerializedName(RestConst.Fields.DUE_TIME)
    private long dueTime;
	@SerializedName(RestConst.Fields.CREATED_TIME)
    private long createdTime;
	@SerializedName(RestConst.Fields.RESOLUTION)
    private int resolution;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getPlanned() {
        return planned;
    }

    public void setPlanned(int planned) {
        this.planned = planned;
    }

    public int getFact() {
        return fact;
    }

    public void setFact(int fact) {
        this.fact = fact;
    }

    public int getSpent() {
        return spent;
    }

    public void setSpent(int spent) {
        this.spent = spent;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    public long getDueTime() {
        return dueTime;
    }

    public void setDueTime(long dueTime) {
        this.dueTime = dueTime;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

}
