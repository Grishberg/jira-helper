package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

import java.io.Serializable;
import java.util.List;

public class Meta implements Serializable {
	@SerializedName(RestConst.Fields.CODE)
	public int code;
}
