package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

import java.io.Serializable;

public class AuthData implements Serializable {
	@SerializedName(RestConst.Fields.REFRESH_TOKEN)
	public String refreshToken;
	@SerializedName(RestConst.Fields.ACCESS_TOKEN)
	public String accessToken;
}
