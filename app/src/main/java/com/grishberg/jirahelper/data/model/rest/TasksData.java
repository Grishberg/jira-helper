package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

import java.util.ArrayList;
import java.util.List;

public class TasksData {
	@SerializedName(RestConst.Fields.TASKS)
	public List<TasksItem> tasks;

}
