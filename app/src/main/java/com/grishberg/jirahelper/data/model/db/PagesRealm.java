package com.grishberg.jirahelper.data.model.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by g on 09.03.16.
 */
public class PagesRealm extends RealmObject {
    public static final int TASKS_PAGE = 1;
    public static final int USERS_PAGE = 2;
    public static final int CURRENT_USER = 100;
    @PrimaryKey
    private int id;
    private int page;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
