package com.grishberg.jirahelper.data.db;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.grishberg.jirahelper.data.model.db.AuthModel;
import com.grishberg.jirahelper.data.model.db.PagesRealm;
import com.grishberg.jirahelper.data.model.db.PersonalDataModel;
import com.grishberg.jirahelper.data.model.db.TaskItemRealm;
import com.grishberg.jirahelper.data.model.db.UserRealm;
import com.grishberg.jirahelper.data.model.rest.AuthData;
import com.grishberg.jirahelper.data.model.rest.TasksItem;
import com.grishberg.jirahelper.framework.db.ListResult;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by grishberg on 08.03.16.
 */
public class TasksDao {
    private static final String TAG = TasksDao.class.getSimpleName();
    private Context context;
    private String accessToken;
    private String refreshToken;
    private Realm realm;

    public TasksDao(Context context) {
        realm = Realm.getDefaultInstance();
    }

    public String getAccessToken() {
        if (accessToken == null) {
            Realm realm = Realm.getDefaultInstance();
            AuthModel authModel = realm.where(AuthModel.class).findFirst();
            if (authModel != null) {
                accessToken = authModel.getAccessToken();
            }
            realm.close();
        }
        return accessToken;
    }

    public String getRefreshToken() {
        if (refreshToken == null) {
            Realm realm = Realm.getDefaultInstance();
            AuthModel authModel = realm.where(AuthModel.class).findFirst();
            if (authModel != null) {
                refreshToken = authModel.getRefreshToken();
            }
            realm.close();
        }
        return refreshToken;
    }

    public void setAuthModel(@NonNull AuthData data) {
        Log.d(TAG, String.format("setAuthModel: a = %s, r = %s", data.accessToken, data.refreshToken));
        AuthModel authModel = new AuthModel();
        authModel.setId(1);
        authModel.setAccessToken(data.accessToken);
        authModel.setRefreshToken(data.refreshToken);
        accessToken = data.accessToken;
        refreshToken = data.refreshToken;
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(authModel);
        realm.commitTransaction();
        realm.close();
    }

    public void setAuth(String login, String password) {
        PersonalDataModel authModel = new PersonalDataModel();
        authModel.setId(1);
        authModel.setLogin(login);
        authModel.setPassword(password);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(authModel);
        realm.commitTransaction();
        realm.close();
    }

    public void addTask(TasksItem item, int page) {
        TaskItemRealm newItem = new TaskItemRealm();
        newItem.setId(item.getId());
        newItem.setKey(item.getKey());
        newItem.setTitle(item.getTitle().trim());
        newItem.setCreatedTime(item.getCreatedTime() * 1000);
        newItem.setDueTime(item.getDueTime() * 1000);
        newItem.setFact(item.getFact());
        newItem.setPlanned(item.getPlanned());
        newItem.setSpent(item.getSpent());
        newItem.setResolution(item.getResolution());
        newItem.setProjectId(item.getProjectId());

        PagesRealm tasksPage = new PagesRealm();
        tasksPage.setId(PagesRealm.TASKS_PAGE);
        tasksPage.setPage(page);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        UserRealm assignee = findOrCreateUser(realm, item.getAssignee().getId(),
                item.getAssignee().getUserName(), false);
        UserRealm customer = findOrCreateUser(realm, item.getCustomer().getId(),
                item.getCustomer().getUserName(), false);
        assignee = realm.copyToRealmOrUpdate(assignee);
        customer = realm.copyToRealmOrUpdate(customer);
        realm.copyToRealmOrUpdate(tasksPage);

        newItem.setAssignee(assignee);
        newItem.setCustomer(customer);

        realm.copyToRealmOrUpdate(newItem);

        realm.commitTransaction();
        realm.close();
    }

    /**
     * find or create user in db
     * @param realm
     * @param id
     * @param name
     * @param isAssignee
     * @return
     */
    private UserRealm findOrCreateUser(Realm realm, int id, String name, boolean isAssignee) {
        UserRealm oldUser = realm.where(UserRealm.class).equalTo("id", id).findFirst();
        if (oldUser != null) {
            oldUser.setUserName(name);
            if (isAssignee) {
                oldUser.setIsAssignee(true);
            }
            return oldUser;
        }
        UserRealm newUser = new UserRealm();
        newUser.setId(id);
        newUser.setIsAssignee(isAssignee);
        newUser.setUserName(name.trim());
        return newUser;
    }

    public ListResult<TaskItemRealm> getPlannedTasks(int userId, long startTime, long endTime) {
        Log.d(TAG, "getPlannedTasks: " + userId);
        RealmQuery<TaskItemRealm> query = null;
        if (userId == 0) {
            query = realm.where(TaskItemRealm.class)
                    .greaterThanOrEqualTo("dueTime", startTime)
                    .lessThan("createdTime", endTime);
        } else {
            query = realm.where(TaskItemRealm.class)
                    .equalTo("assignee.id", userId)
                    .greaterThanOrEqualTo("dueTime", startTime)
                    .lessThan("createdTime", endTime);
        }
        return new ListResultImpl<>(query
                .findAllSorted("dueTime", Sort.DESCENDING, "createdTime", Sort.DESCENDING));
    }

    public void clearTasks(int userId) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<TaskItemRealm> results = null;
        if (userId == 0) {
            results = realm.where(TaskItemRealm.class).findAll();
        } else {
            results = realm.where(TaskItemRealm.class).equalTo("assignee.id", userId).findAll();
        }
        RealmResults<PagesRealm> pages = realm.where(PagesRealm.class)
                .equalTo("id", PagesRealm.TASKS_PAGE).findAll();
        realm.beginTransaction();
        results.clear();
        pages.clear();
        realm.commitTransaction();
        realm.close();
    }

    public int getLastTaskPage() {
        int result = -1;
        Realm realm = Realm.getDefaultInstance();
        PagesRealm page = realm.where(PagesRealm.class).equalTo("id", PagesRealm.TASKS_PAGE).findFirst();
        if (page != null) {
            result = page.getPage();
        }
        realm.close();
        return result;
    }

    /*
    public ListResult<ScheduleModel> getScheduleList() {
        return new ListResultImpl<ScheduleModel>(realm.where(ScheduleModel.class)
                .findAllSorted("time"));
    }

    public void addSchedule(ScheduleModel scheduleModel) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(scheduleModel);
        realm.commitTransaction();
        realm.close();
    }

    public void addSchedule(List<ScheduleModel> scheduleModels) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(scheduleModels);
        realm.commitTransaction();
        realm.close();
    }

    public void removeSchedule(int id) {
        Realm realm = Realm.getInstance(context);
        ScheduleModel model = realm.where(ScheduleModel.class).equalTo("id", id).findFirst();
        if (model != null) {
            realm.beginTransaction();
            model.removeFromRealm();
            realm.commitTransaction();
        }
        realm.close();
    }

    public void addAuth(String login, String password, String token, int role) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        AuthModel authModel = new AuthModel();
        authModel.setId(1);
        authModel.setLogin(login);
        authModel.setPassword(password);
        authModel.setRole(role);
        authModel.setToken(token);
        realm.copyToRealmOrUpdate(authModel);
        realm.commitTransaction();
        realm.close();
    }



    public AuthModel getAuth() {
        Realm realm = Realm.getInstance(context);
        AuthModel authModel = realm.where(AuthModel.class).equalTo("id", 1).findFirst();
        realm.close();
        if(authModel != null) {
            AuthModel clone = new AuthModel();
            clone.setLogin(authModel.getLogin());
            clone.setPassword(authModel.getPassword());
            clone.setRole(authModel.getRole());
            clone.setToken(authModel.getToken());
            return clone;
        }
        return null;
    }

    public void setAuth(AuthModel authModel) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(authModel);
        realm.commitTransaction();
        realm.close();
    }
    */
    public void release() {
        if (realm != null) {
            realm.close();
        }
    }
}
