package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;
import com.grishberg.jirahelper.data.rest.RestConst;

/**
 * Created by g on 10.03.16.
 */
public class UsersItem {
    private int id;
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
