package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

import java.io.Serializable;

public class AuthResponse implements Serializable {
	@SerializedName(RestConst.Fields.META)
	public Meta meta;
	@SerializedName(RestConst.Fields.DATA)
	public AuthData data;

}
