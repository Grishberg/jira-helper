package com.grishberg.jirahelper.data.model.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by grishberg on 09.03.16.
 */
public class UserRealm extends RealmObject {
    private static final String TAG = UserRealm.class.getSimpleName();
    @PrimaryKey
    private int id;
    private String userName;
    private boolean isAssignee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isAssignee() {
        return isAssignee;
    }

    public void setIsAssignee(boolean isAssignee) {
        this.isAssignee = isAssignee;
    }
}
