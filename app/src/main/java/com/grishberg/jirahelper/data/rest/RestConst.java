package com.grishberg.jirahelper.data.rest;

public final class RestConst {
    public static final String BACKEND_ENDPOINT = "http://10.102.100.151:8081/";
    public static final String API = "api/v1/";
    public static final String CLIENT_ID_VAL = "SDjsfhj";
    public static final String CLIENT_SECRET_VAL = "kadluadn";

    //methods
    public static final class Methods {
        public static final String AUTH = API + "auth/{login}/{password}/{client_id}/{client_secret}";
        public static final String REFRESH_TOKEN = API + "refreshToken/{refresh_token}/{client_id}/{client_secret}";
        public static final String USERS = API + "users/{page}/{access_token}";
        public static final String PLANNED_TASKS = API + "plannedTasks/{start_time}/{end_time}/{user_id}/{page}/{access_token}";
        public static final String UPDATE = API + "update/{access_token}";
    }

    // parameters
    public static final class Parameters {
        public static final String ACCESS_TOKEN = "access_token";
        public static final String LOGIN = "login";
        public static final String PASSWORD = "password";
        public static final String CLIENT_ID = "client_id";
        public static final String CLIENT_SECRET = "client_secret";
        public static final String REFRESH_TOKEN = "refresh_token";
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
        public static final String USER_ID = "user_id";
        public static final String PAGE = "page";
    }

    //fields
    public static final class Fields {
        public static final String ID = "id";
        public static final String USER_NAME = "userName";
        public static final String META = "meta";
        public static final String DATA = "data";
        public static final String CODE = "code";
        public static final String REFRESH_TOKEN = "refreshToken";
        public static final String ACCESS_TOKEN = "accessToken";
        public static final String PAGINATION = "pagination";
        public static final String KEY = "key";
        public static final String TITLE = "title";
        public static final String PROJECT_ID = "projectId";
        public static final String PLANNED = "planned";
        public static final String FACT = "fact";
        public static final String SPENT = "spent";
        public static final String CUSTOMER = "customer";
        public static final String ASSIGNEE = "assignee";
        public static final String DUE_TIME = "dueTime";
        public static final String CREATED_TIME = "createdTime";
        public static final String RESOLUTION = "resolution";
        public static final String TASKS = "tasks";
        public static final String NEXT = "next";
        public static final String PREV = "prev";
        public static final String USERS = "users";
    }

    public static final class Errors {
        public static final int CONNECTION_ERROR = -1;
    }

    private RestConst() {
    }
}
