package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;
import com.grishberg.jirahelper.data.rest.RestConst;

import java.util.List;

/**
 * Created by g on 10.03.16.
 */
public class UsersData {
    public List<UsersItem> users;
}
