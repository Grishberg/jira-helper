package com.grishberg.jirahelper.data.rest.workers;

import android.text.TextUtils;
import android.util.Log;

import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.model.rest.AuthResponse;
import com.grishberg.jirahelper.data.model.rest.TasksItem;
import com.grishberg.jirahelper.data.model.rest.TasksResponse;
import com.grishberg.jirahelper.data.rest.RestConst;
import com.grishberg.jirahelper.data.rest.retrofit_services.RestRequests;
import com.grishberg.jirahelper.framework.BaseRestRequest;

import java.io.IOException;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by grishberg on 08.03.16.
 */
public class ApiRequests {
    private static final String TAG = ApiRequests.class.getSimpleName();

    public static BaseRestRequest plannedTasks(final long startTime, final long endTime,
                                               final int userId, final int page,
                                               final String accessToken,
                                               final TasksDao taskDao) {
        return new BaseRestRequest<TasksResponse, RestRequests, Integer>() {
            @Override
            public Response onRequest(RestRequests service) throws IOException {
                Call<TasksResponse> resp = service.plannedTasks(startTime, endTime,
                        userId, page, accessToken);
                Response result = resp.execute();
                return result;
            }

            @Override
            public Integer onSuccess(final TasksResponse response) {
                super.onSuccess(response);
                Integer nextPage = -1;

                if (response != null) {
                    if (response.pagination != null &&
                            !TextUtils.isEmpty(response.pagination.next)) {
                        nextPage = Integer.valueOf(response.pagination.next);
                    }

                    if (response.data != null) {
                        if (page == 0) {
                            //clear old data
                            Log.d(TAG, "onSuccess: clear all data");
                            taskDao.clearTasks(userId);
                        }
                        // store to DB
                        if (response.data != null && response.data.tasks != null) {
                            for (TasksItem item : response.data.tasks) {
                                taskDao.addTask(item, nextPage);
                            }
                        }
                    }
                }
                return nextPage;
            }
        };
    }
}
