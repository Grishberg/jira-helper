package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

public class TasksResponse {
	@SerializedName(RestConst.Fields.META)
	public Meta meta;
	@SerializedName(RestConst.Fields.DATA)
	public TasksData data;
	@SerializedName(RestConst.Fields.PAGINATION)
	public Pagination pagination;

}
