package com.grishberg.jirahelper.data.rest.model;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

import java.util.List;

public class Meta {
	@SerializedName(RestConst.Fields.CODE)
	public int code;

}
