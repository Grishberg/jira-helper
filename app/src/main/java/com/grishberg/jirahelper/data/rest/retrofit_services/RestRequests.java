package com.grishberg.jirahelper.data.rest.retrofit_services;

import com.grishberg.jirahelper.data.model.rest.TasksResponse;
import com.grishberg.jirahelper.data.model.rest.UsersResponse;
import com.grishberg.jirahelper.data.rest.RestConst;
import com.grishberg.jirahelper.data.model.rest.AuthResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by grishberg on 07.03.16.
 */
public interface RestRequests {
    @GET(RestConst.Methods.AUTH)
    Call<AuthResponse> auth(@Path(RestConst.Parameters.LOGIN) String login,
                            @Path(RestConst.Parameters.PASSWORD) String password,
                            @Path(RestConst.Parameters.CLIENT_ID) String clientId,
                            @Path(RestConst.Parameters.CLIENT_SECRET) String clientSecret);

    @GET(RestConst.Methods.REFRESH_TOKEN)
    Call<AuthResponse> refreshToken(@Path(RestConst.Parameters.REFRESH_TOKEN) String refreshToken,
                                    @Path(RestConst.Parameters.CLIENT_ID) String clientId,
                                    @Path(RestConst.Parameters.CLIENT_SECRET) String clientSecret
    );

    @GET(RestConst.Methods.PLANNED_TASKS)
    Call<TasksResponse> plannedTasks(@Path(RestConst.Parameters.START_TIME) long startTime,
                                    @Path(RestConst.Parameters.END_TIME) long endTime,
                                    @Path(RestConst.Parameters.USER_ID) int userId,
                                    @Path(RestConst.Parameters.PAGE) int page,
                                    @Path(RestConst.Parameters.ACCESS_TOKEN) String accessToken );

    @GET(RestConst.Methods.USERS)
    Call<UsersResponse> getUsers(@Path(RestConst.Parameters.PAGE) int page,
                                    @Path(RestConst.Parameters.ACCESS_TOKEN) String accessToken );


}
