package com.grishberg.jirahelper.data.rest.workers;

import com.grishberg.jirahelper.data.rest.RestConst;
import com.grishberg.jirahelper.data.model.rest.AuthResponse;
import com.grishberg.jirahelper.data.rest.retrofit_services.RestRequests;
import com.grishberg.jirahelper.framework.BaseRestRequest;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by grishberg on 08.03.16.
 */
public class AuthRequests {
    private static final String TAG = AuthRequests.class.getSimpleName();
    public static BaseRestRequest auth(final String login, final String password){
        return new BaseRestRequest<AuthResponse, RestRequests, AuthResponse>() {
            @Override
            public Response onRequest(RestRequests service) throws IOException {
                Call<AuthResponse> resp = service.auth(login, password,
                        RestConst.CLIENT_ID_VAL, RestConst.CLIENT_SECRET_VAL);
                Response result = resp.execute();
                return result;
            }

            @Override
            public AuthResponse onSuccess(AuthResponse response) {
                return response;
            }
        };
    }
}
