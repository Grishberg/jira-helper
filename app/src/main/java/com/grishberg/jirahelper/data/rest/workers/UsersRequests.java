package com.grishberg.jirahelper.data.rest.workers;

import android.text.TextUtils;
import android.util.Log;

import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.db.UsersDao;
import com.grishberg.jirahelper.data.model.rest.TasksItem;
import com.grishberg.jirahelper.data.model.rest.TasksResponse;
import com.grishberg.jirahelper.data.model.rest.UsersItem;
import com.grishberg.jirahelper.data.model.rest.UsersResponse;
import com.grishberg.jirahelper.data.rest.retrofit_services.RestRequests;
import com.grishberg.jirahelper.framework.BaseRestRequest;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * реквесты для извлечения пользователей
 * Created by g on 10.03.16.
 */
public class UsersRequests {
    private static final String TAG = UsersRequests.class.getSimpleName();

    public static BaseRestRequest getUsers(final int page,
                                           final String accessToken,
                                           final UsersDao usersDao) {
        return new BaseRestRequest<UsersResponse, RestRequests, Integer>() {
            @Override
            public Response onRequest(RestRequests service) throws IOException {
                Call<UsersResponse> resp = service.getUsers(page, accessToken);
                Response result = resp.execute();
                return result;
            }

            @Override
            public Integer onSuccess(final UsersResponse response) {
                super.onSuccess(response);
                Log.d(TAG, "onSuccess: ");
                Integer nextPage = -1;

                if (response != null) {
                    if (response.pagination != null &&
                            !TextUtils.isEmpty(response.pagination.next)) {
                        nextPage = Integer.valueOf(response.pagination.next);
                    }

                    if (response.data != null) {
                        // store to DB
                        usersDao.addUsers(response.data.users, nextPage);
                    }
                }
                return nextPage;
            }
        };
    }
}
