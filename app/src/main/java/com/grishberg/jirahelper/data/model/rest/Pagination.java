package com.grishberg.jirahelper.data.model.rest;

import com.google.gson.annotations.SerializedName;

import com.grishberg.jirahelper.data.rest.RestConst;

import java.util.List;

public class Pagination {
	@SerializedName(RestConst.Fields.NEXT)
	public String next;
	@SerializedName(RestConst.Fields.PREV)
	public String prev;

}
