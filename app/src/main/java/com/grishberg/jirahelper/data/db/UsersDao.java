package com.grishberg.jirahelper.data.db;

import android.content.Context;
import android.util.Log;

import com.grishberg.jirahelper.data.model.db.PagesRealm;
import com.grishberg.jirahelper.data.model.db.UserRealm;
import com.grishberg.jirahelper.data.model.rest.UsersItem;
import com.grishberg.jirahelper.framework.db.ListResult;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by g on 10.03.16.
 */
public class UsersDao {
    private static final String TAG = UsersDao.class.getSimpleName();
    private Context context;
    private String accessToken;
    private String refreshToken;
    private Realm realm;

    public UsersDao(Context context) {
        realm = Realm.getDefaultInstance();
    }

    public void addUsers(List<UsersItem> items, int page) {
        Log.d(TAG, String.format("addUsers: next page = %d", page));
        PagesRealm tasksPage = new PagesRealm();
        tasksPage.setId(PagesRealm.USERS_PAGE);
        tasksPage.setPage(page);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        for (UsersItem item : items) {
            UserRealm assignee = new UserRealm();
            assignee.setId(item.getId());
            assignee.setIsAssignee(true);
            assignee.setUserName(item.getUserName().trim());
            realm.copyToRealmOrUpdate(assignee);
        }
        realm.copyToRealmOrUpdate(tasksPage);
        realm.commitTransaction();
        realm.close();
    }

    public ListResult<UserRealm> getUsers() {
        return new ListResultImpl<>(realm.where(UserRealm.class)
                .equalTo("isAssignee", true)
                .findAllSorted("userName", Sort.ASCENDING));
    }

    public void clearUsers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<UserRealm> results = realm.where(UserRealm.class).findAll();

        RealmResults<PagesRealm> pages = realm.where(PagesRealm.class)
                .equalTo("id", PagesRealm.USERS_PAGE).findAll();
        realm.beginTransaction();
        results.clear();
        pages.clear();
        realm.commitTransaction();
        realm.close();
    }

    public int getLastUserPage() {
        int result = -1;
        Realm realm = Realm.getDefaultInstance();
        PagesRealm page = realm.where(PagesRealm.class).equalTo("id", PagesRealm.USERS_PAGE).findFirst();
        if (page != null) {
            result = page.getPage();
        }
        realm.close();
        return result;
    }

    public int getCurrentUser() {
        int result = -1;
        Realm realm = Realm.getDefaultInstance();
        PagesRealm page = realm.where(PagesRealm.class).equalTo("id", PagesRealm.CURRENT_USER).findFirst();
        if (page != null) {
            result = page.getPage();
        }
        realm.close();
        return result;
    }

    public int setCurrentUser(int userId) {
        int result = -1;
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        PagesRealm tasksPage = new PagesRealm();
        tasksPage.setId(PagesRealm.CURRENT_USER);
        tasksPage.setPage(userId);

        realm.commitTransaction();
        realm.close();
        return result;
    }

    public void release() {
        if (realm != null) {
            realm.close();
        }
    }
}
