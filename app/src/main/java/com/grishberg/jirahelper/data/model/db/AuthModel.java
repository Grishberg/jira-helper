package com.grishberg.jirahelper.data.model.db;

import com.google.gson.annotations.SerializedName;
import com.grishberg.jirahelper.data.rest.RestConst;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by grishberg on 08.03.16.
 */
public class AuthModel extends RealmObject {
    private static final String TAG = AuthModel.class.getSimpleName();
    @PrimaryKey
    private int id;
    private String refreshToken;
    private String accessToken;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
