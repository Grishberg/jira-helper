package com.grishberg.jirahelper.data.rest;

import android.content.Context;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.model.db.AuthModel;
import com.grishberg.jirahelper.data.model.rest.AuthResponse;
import com.grishberg.jirahelper.data.rest.exceptions.UnauthorizedException;
import com.grishberg.jirahelper.data.rest.retrofit_services.RestRequests;
import com.grishberg.jirahelper.framework.BaseRestRequest;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;

import io.realm.RealmObject;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by grishberg on 08.03.16.
 */
public class RestWorker {
    private static final String TAG = RestWorker.class.getSimpleName();
    public static final int TRIES_COUNT = 5;
    private static volatile boolean isRefreshing = false;
    private static volatile Object monitor = new Object();
    private RestRequests restService;
    private ResponseListener responseListener;
    private TasksDao tasksDao;
    private Context context;

    public RestWorker(Context context, ResponseListener responseListener) {
        createService();
        this.responseListener = responseListener;
        this.tasksDao = new TasksDao(context);
        this.context = context;
    }

    private void createService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestConst.BACKEND_ENDPOINT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(
                        new Gson()))
                .addConverterFactory(GsonConverterFactory.create(
                        GsonConverterFactoryHelper.create()))
                .build();
        restService = retrofit.create(RestRequests.class);
    }

    /**
     * send request to server in new thread
     *
     * @param request
     */
    public void sendRequest(final BaseRestRequest request) {
        request.setContext(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                try {
                    executeRequest(request);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void executeRequest(BaseRestRequest request) throws InterruptedException {
        Object result = null;
        Object attachment = null;
        int errorCode = 0;
        String errorMessage = null;
        request.setAuthToken(tasksDao.getAccessToken());
        for (int tries = 0; tries < TRIES_COUNT; tries++) {
            try {
                Log.d(TAG, "[REST] executeRequest: ");
                Response response = request.onRequest(restService);
                result = response != null ? response.body() : null;
                //result.setCode(response.code());
                Log.d(TAG, String.format("executeRequest: code = %d", response.code()));
                switch (response.code()) {
                    case 200:
                        attachment = request.onSuccess(result);
                        if (responseListener != null) {
                            responseListener.onSuccess(attachment, request.getRequestId());
                        }
                        return;
                    case 401:
                        throw new UnauthorizedException();
                }
            } catch (UnauthorizedException e) {
                // refreshToken
                if (!refreshToken()) {
                    break;
                }
            } catch (ConnectException e) {
                errorCode = RestConst.Errors.CONNECTION_ERROR;
                errorMessage = e.getMessage();
                Log.e(TAG, "executeRequest: ", e);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        responseListener.onFail(errorCode, errorMessage, request.getRequestId());
    }

    /**
     * Обновление токена
     *
     * @return true - if success, false if need update refresh token
     */
    public boolean refreshToken() throws InterruptedException {
        if (isRefreshing) {
            Log.d(TAG, "refreshToken: wait while refresh token");
            synchronized (monitor) {
                while (isRefreshing) {
                    monitor.wait();
                }
            }
        } else {
            isRefreshing = true;
            try {
                Log.d(TAG, "refreshToken: ");
                String refreshToken = tasksDao.getRefreshToken();
                if (refreshToken == null) {
                    return false;
                }
                Call<AuthResponse> resp = restService.refreshToken(refreshToken,
                        RestConst.CLIENT_ID_VAL,
                        RestConst.CLIENT_SECRET_VAL
                );
                Response<AuthResponse> result = resp.execute();
                AuthResponse body = result.body();
                if (body != null && body.data != null) {
                    tasksDao.setAuthModel(body.data);
                }
            } catch (Exception e) {
                Log.d(TAG, "refreshToken: exception: " + e.getMessage());
                return false;
            }
            synchronized (monitor) {
                monitor.notifyAll();
            }
        }

        return true;
    }

    public static class GsonConverterFactoryHelper {
        public static final String TAG = GsonConverterFactoryHelper.class.getName();

        public static Gson create() {
            return new GsonBuilder()
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaringClass().equals(RealmObject.class);
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .create();
        }
    }

    public interface ResponseListener {
        void onSuccess(Object o, int requestId);

        void onFail(int code, String message, int requestId);
    }
}