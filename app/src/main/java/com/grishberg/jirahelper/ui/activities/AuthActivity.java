package com.grishberg.jirahelper.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.grishberg.jirahelper.R;
import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.model.rest.AuthResponse;
import com.grishberg.jirahelper.data.rest.workers.AuthRequests;
import com.grishberg.jirahelper.data.services.RestService;
import com.grishberg.jirahelper.framework.BaseBinderActivity;
import com.grishberg.jirahelper.framework.RestCallback;

public class AuthActivity extends BaseBinderActivity<RestService> {

    private static final String TAG = AuthActivity.class.getSimpleName();

    private EditText etLogin;
    private EditText etPassword;
    private Button btSubmit;
    private TasksDao tasksDao;

    @Override
    protected Intent getServiceIntent() {
        return new Intent(getApplicationContext(), RestService.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_auth);

        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btSubmit = (Button) findViewById(R.id.btSubmit);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = etLogin.getText().toString();
                String password = etPassword.getText().toString();
                Log.d(TAG, "onClick: ");
                if (login.length() > 0 && password.length() > 0) {
                    //TODO: send auth request
                    btSubmit.setEnabled(false);
                    doAuth(view, login, password);
                } else {
                    Snackbar.make(view, R.string.auth_empty_login_msg, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        tasksDao = new TasksDao(this);
        // если в базе есть рефреш токен, то можно запускать основную активность
        if(tasksDao.getRefreshToken() != null){
            startMainActivity();
        }
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void doAuth(final View view, String login, String password) {
        Log.d(TAG, "doAuth: ");
        sendRequest(AuthRequests.auth(login, password), new RestCallback<AuthResponse>() {
            @Override
            public void onResponse(AuthResponse response) {
                Log.d(TAG, "onResponse: ");
                if(response != null && response.data != null) {
                    tasksDao.setAuth(etLogin.getText().toString(),
                            etPassword.getText().toString());
                    tasksDao.setAuthModel(response.data);
                    startMainActivity();
                }
            }

            @Override
            public void onFail(int code, String message) {
                Snackbar.make(view, R.string.auth_wrong_login, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                btSubmit.setEnabled(true);
            }
        });
    }
}
