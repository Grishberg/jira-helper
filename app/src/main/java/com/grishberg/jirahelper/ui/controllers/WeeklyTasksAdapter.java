package com.grishberg.jirahelper.ui.controllers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grishberg.jirahelper.R;
import com.grishberg.jirahelper.data.model.db.TaskItemRealm;
import com.grishberg.jirahelper.framework.db.ListResult;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by g on 11.03.16.
 */
public class WeeklyTasksAdapter extends RecyclerView.Adapter<WeeklyTasksAdapter.WeeklyTaskViewHolder> {
    private static final String TAG = TasksAdapter.class.getSimpleName();
    private ListResult<TaskItemRealm> listResult;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    public WeeklyTasksAdapter(ListResult<TaskItemRealm> listResult) {
        this.listResult = listResult;
    }

    @Override
    public WeeklyTaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task_cell
                , parent, false);
        WeeklyTaskViewHolder pvh = new WeeklyTaskViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(WeeklyTaskViewHolder holder, int position) {
        TaskItemRealm item = listResult.getItem(position);
        holder.tvTitle.setText(item.getTitle());
        holder.id = item.getId();
        holder.position = position;
        holder.tvCreatedTime.setText(item.getCreatedTime() == 0 ? "" :
                sdf.format(new Date(item.getCreatedTime())));
        holder.tvDueTime.setText(item.getDueTime() == 0 ? "" :
                sdf.format(new Date(item.getDueTime())));
        holder.tvPlanned.setText(String.format("%.2f", (float) item.getPlanned() / 3600.));
        holder.tvFact.setText(String.format("%.2f", (float) item.getFact() / 3600.));
        holder.tvSpent.setText(String.format("%.2f", (float) item.getSpent() / 3600.));
    }

    @Override
    public int getItemCount() {
        return listResult.getCount();
    }

    public static class WeeklyTaskViewHolder extends RecyclerView.ViewHolder {
        public int id;
        public int position;
        TextView tvTitle;
        TextView tvCreatedTime;
        TextView tvDueTime;
        TextView tvPlanned;
        TextView tvFact;
        TextView tvSpent;

        WeeklyTaskViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvCreatedTime = (TextView) itemView.findViewById(R.id.tvCreatedTime);
            tvDueTime = (TextView) itemView.findViewById(R.id.tvDueTime);

            tvPlanned = (TextView) itemView.findViewById(R.id.tvPlanned);
            tvFact = (TextView) itemView.findViewById(R.id.tvFact);
            tvSpent = (TextView) itemView.findViewById(R.id.tvSpent);
        }
    }
}
