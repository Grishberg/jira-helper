package com.grishberg.jirahelper.ui.controllers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.grishberg.jirahelper.R;
import com.grishberg.jirahelper.data.model.db.UserRealm;
import com.grishberg.jirahelper.framework.db.ListResult;
import com.grishberg.jirahelper.interfaces.OnUserClickListener;

/**
 * Created by g on 10.03.16.
 */
public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder>
        implements OnUserClickListener {
    private static final String TAG = TasksAdapter.class.getSimpleName();
    private ListResult<UserRealm> listResult;
    private OnUserClickListener listener;

    public UsersAdapter(ListResult<UserRealm> listResult) {
        this.listResult = listResult;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_user_cell
                , parent, false);
        UserViewHolder pvh = new UserViewHolder(v);
        pvh.setListener(this);
        return pvh;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        UserRealm item = listResult.getItem(position);
        holder.tvName.setText(item.getUserName());
        holder.id = item.getId();
        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return listResult.getCount();
    }

    public void setListener(OnUserClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onUserClicked(int userId) {
        if (listener != null) {
            listener.onUserClicked(userId);
        }
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public int id;
        public int position;
        TextView tvName;
        OnUserClickListener listener;

        UserViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onUserClicked(id);
                    }
                }
            });
        }

        public void setListener(OnUserClickListener listener) {
            this.listener = listener;
        }
    }
}
