package com.grishberg.jirahelper.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grishberg.jirahelper.R;
import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.db.UsersDao;
import com.grishberg.jirahelper.data.rest.workers.ApiRequests;
import com.grishberg.jirahelper.data.rest.workers.UsersRequests;
import com.grishberg.jirahelper.framework.RestCallback;
import com.grishberg.jirahelper.interfaces.FragmentInteractionListener;
import com.grishberg.jirahelper.interfaces.OnFirstBoundListener;
import com.grishberg.jirahelper.interfaces.OnUserClickListener;
import com.grishberg.jirahelper.ui.controllers.EndlessRecyclerOnScrollListener;
import com.grishberg.jirahelper.ui.controllers.UsersAdapter;

public class UsersFragment extends Fragment implements OnFirstBoundListener, OnUserClickListener {
    private static final String TAG = UsersFragment.class.getSimpleName();
    private FragmentInteractionListener mListener;
    private RecyclerView rvUsers;
    private EndlessRecyclerOnScrollListener scrollListener;
    private UsersDao usersDao;
    private TasksDao tasksDao;
    private UsersAdapter usersAdapter;

    public UsersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UsersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UsersFragment newInstance() {
        UsersFragment fragment = new UsersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        usersDao = new UsersDao(getContext());
        tasksDao = new TasksDao(getContext());
        usersAdapter = new UsersAdapter(usersDao.getUsers());
        usersAdapter.setListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_users, container, false);

        initWidgets(view);
        return view;
    }

    // инициализация recycler view
    private void initWidgets(View view) {
        rvUsers = (RecyclerView) view.findViewById(R.id.rvUsers);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rvUsers.setLayoutManager(llm);
        scrollListener = new EndlessRecyclerOnScrollListener(llm) {
            @Override
            public void onLoadMore(int current_page) {
                int nextPage = usersDao.getLastUserPage();
                Log.d(TAG, String.format("onLoadMore: nextPage = %d, page = %d", nextPage, current_page));
                if (nextPage < 0) return;
                getNextUserPage(nextPage);
            }
        };
        Log.d(TAG, "initWidgets: addOnScrollListener " + scrollListener);
        rvUsers.addOnScrollListener(scrollListener);
        rvUsers.setAdapter(usersAdapter);
        usersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnUserFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (usersAdapter != null) {
            usersAdapter.setListener(null);
        }
        if(usersDao != null) {
            usersDao.release();
        }
        if(tasksDao != null){
            tasksDao.release();
        }
    }

    @Override
    public void onFirstBound() {
        Log.d(TAG, "onFirstBound: ");
        getNextUserPage(0);
    }

    /**
     * download next page
     *
     * @param nextPage
     */
    private void getNextUserPage(int nextPage) {
        Log.d(TAG, String.format("getNextTaskPage: page = %d", nextPage));
        mListener.onSendRequest(UsersRequests.getUsers(nextPage, tasksDao.getAccessToken(),
                        usersDao
                ),
                new RestCallback<Integer>() {
                    @Override
                    public void onResponse(Integer nextPage) {
                        scrollListener.resetLoadingFlag();
                        Log.d(TAG, "onResponse: next page = " + nextPage);
                        usersAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFail(int code, String message) {
                    }
                });
    }

    @Override
    public void onUserClicked(int userId) {
        if (mListener != null) {
            mListener.onUserSelected(userId);
        }
    }
}
