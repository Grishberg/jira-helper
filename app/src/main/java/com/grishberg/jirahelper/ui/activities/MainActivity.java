package com.grishberg.jirahelper.ui.activities;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.grishberg.jirahelper.R;
import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.db.UsersDao;
import com.grishberg.jirahelper.data.model.db.PagesRealm;
import com.grishberg.jirahelper.data.rest.workers.ApiRequests;
import com.grishberg.jirahelper.data.services.RestService;
import com.grishberg.jirahelper.framework.BaseBinderActivity;
import com.grishberg.jirahelper.framework.BaseRestRequest;
import com.grishberg.jirahelper.framework.RestCallback;
import com.grishberg.jirahelper.interfaces.FragmentInteractionListener;
import com.grishberg.jirahelper.interfaces.OnFirstBoundListener;
import com.grishberg.jirahelper.ui.controllers.EndlessRecyclerOnScrollListener;
import com.grishberg.jirahelper.ui.controllers.TasksAdapter;
import com.grishberg.jirahelper.ui.fragments.TasksFragment;
import com.grishberg.jirahelper.ui.fragments.UsersFragment;

import java.util.Calendar;

public class MainActivity extends BaseBinderActivity<RestService>
        implements NavigationView.OnNavigationItemSelectedListener,
        FragmentInteractionListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final int BACKPRESS_DELAY_MILLIS = 2000;
    private OnFirstBoundListener currentFragment;
    private UsersDao usersDao;
    private Handler doubleBackPressHandler;
    private boolean isBackPressed;

    @Override
    protected Intent getServiceIntent() {
        return new Intent(getApplicationContext(), RestService.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.setVisibility(View.GONE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        usersDao = new UsersDao(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (usersDao.getCurrentUser() < 0) {
            // отобразить список пользователей
            currentFragment = UsersFragment.newInstance();
            fragmentTransaction.add(R.id.vContent,
                    (Fragment) currentFragment,
                    UsersFragment.class.getSimpleName());
        } else {
            // отобразить список задач для пользователя
            currentFragment = TasksFragment.newInstance(usersDao.getCurrentUser());
            fragmentTransaction.add(R.id.vContent,
                    (Fragment) currentFragment,
                    UsersFragment.class.getSimpleName());
        }
        fragmentTransaction.commit();
        doubleBackPressHandler = new Handler();
    }

    @Override
    protected void onFirstBound() {
        super.onFirstBound();
        Log.d(TAG, "onFirstBound: ");
        if (currentFragment != null) {
            currentFragment.onFirstBound();
        }
    }

    private Runnable backPressedRunnable = new Runnable() {
        @Override
        public void run() {
            isBackPressed = false;
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (currentFragment instanceof TasksFragment) {
            setUsersFragment();
        } else {
            if (isBackPressed) {
                super.onBackPressed();
            } else {
                isBackPressed = true;
                doubleBackPressHandler.postDelayed(backPressedRunnable, BACKPRESS_DELAY_MILLIS);
                Snackbar.make(drawer, R.string.second_back_press_for_exit, Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_users) {
            // Handle the camera action
            setUsersFragment();
        } else if (id == R.id.nav_tasks) {
            setTasksFragment();
        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setTasksFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        currentFragment = TasksFragment.newInstance(0);
        fragmentTransaction.replace(R.id.vContent,
                (Fragment) currentFragment,
                TasksFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }

    private void setUsersFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        currentFragment = UsersFragment.newInstance();
        fragmentTransaction.replace(R.id.vContent,
                (Fragment) currentFragment,
                UsersFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }


    @Override
    public void onTaskSelected(int taskId) {

    }

    /**
     * событие клика на пользователе
     * @param userId
     */
    @Override
    public void onUserSelected(int userId) {
        Log.d(TAG, "onUserSelected: " + userId);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        usersDao.setCurrentUser(userId);
        currentFragment = TasksFragment.newInstance(userId);
        fragmentTransaction.replace(R.id.vContent,
                (Fragment) currentFragment,
                TasksFragment.class.getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public boolean onSendRequest(BaseRestRequest request, RestCallback callback) {
        return sendRequest(request, callback);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        if (usersDao != null) {
            usersDao.release();
        }
        if(doubleBackPressHandler != null){
            doubleBackPressHandler.removeCallbacks(backPressedRunnable);
        }
    }
}
