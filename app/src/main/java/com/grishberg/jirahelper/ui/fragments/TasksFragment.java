package com.grishberg.jirahelper.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grishberg.jirahelper.R;
import com.grishberg.jirahelper.data.db.TasksDao;
import com.grishberg.jirahelper.data.rest.workers.ApiRequests;
import com.grishberg.jirahelper.framework.RestCallback;
import com.grishberg.jirahelper.interfaces.FragmentInteractionListener;
import com.grishberg.jirahelper.interfaces.OnFirstBoundListener;
import com.grishberg.jirahelper.ui.controllers.EndlessRecyclerOnScrollListener;
import com.grishberg.jirahelper.ui.controllers.TasksAdapter;

import java.util.Calendar;

public class TasksFragment extends Fragment implements OnFirstBoundListener {
    private static final String ARG_PARAM_USER_ID = "arg_param_user_id";
    private static final String TAG = TasksFragment.class.getSimpleName();
    private int userId;
    private FragmentInteractionListener mListener;
    private RecyclerView rvTasks;
    private TasksDao tasksDao;
    private TasksAdapter tasksAdapter;
    private EndlessRecyclerOnScrollListener scrollListener;
    private long startTime;
    private long endTime;

    public TasksFragment() {
        // Required empty public constructor
    }

    /**
     * @param userId id пользователя.
     * @return A new instance of fragment TasksFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TasksFragment newInstance(int userId) {
        TasksFragment fragment = new TasksFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM_USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getInt(ARG_PARAM_USER_ID, 0);
        }

        Calendar startCal = Calendar.getInstance();
        startCal.add(Calendar.DAY_OF_MONTH, -7);
        startCal.set(Calendar.HOUR_OF_DAY, 0);
        startCal.set(Calendar.MINUTE, 0);
        startCal.set(Calendar.SECOND, 0);
        startCal.set(Calendar.MILLISECOND, 0);

        Calendar endCal = Calendar.getInstance();
        endCal.add(Calendar.DAY_OF_MONTH, 7);

        startTime = startCal.getTimeInMillis() / 1000 * 0;
        endTime = endCal.getTimeInMillis() / 1000;

        tasksDao = new TasksDao(getContext());
        tasksAdapter = new TasksAdapter(tasksDao.getPlannedTasks(userId, startTime * 1000, endTime * 1000));
    }

    @Override
    public void onFirstBound() {
        //getNextTaskPage(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        getNextTaskPage(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tasks, container, false);
        initWidgets(view);
        return view;
    }

    private void initWidgets(View view) {
        rvTasks = (RecyclerView) view.findViewById(R.id.rvTasks);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rvTasks.setLayoutManager(llm);
        scrollListener = new EndlessRecyclerOnScrollListener(llm) {
            @Override
            public void onLoadMore(int current_page) {
                int nextPage = tasksDao.getLastTaskPage();
                Log.d(TasksFragment.TAG, String.format("onLoadMore: nextPage = %d, page = %d", nextPage, current_page));
                if (nextPage < 0) return;
                getNextTaskPage(nextPage);
            }
        };
        Log.d(TAG, "initWidgets: addOnScrollListener " + scrollListener);
        rvTasks.addOnScrollListener(scrollListener);
        rvTasks.setAdapter(tasksAdapter);
        tasksAdapter.notifyDataSetChanged();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onTaskSelected(int taskId) {
        if (mListener != null) {
            mListener.onTaskSelected(taskId);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnTaskFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(tasksDao != null){
            tasksDao.release();
            tasksDao = null;
        }
    }

    /**
     * download next page
     *
     * @param nextPage
     */
    private void getNextTaskPage(int nextPage) {
        Log.d(TAG, String.format("getNextTaskPage: page = %d", nextPage));
        mListener.onSendRequest(ApiRequests.plannedTasks(startTime, endTime,
                        userId, nextPage, tasksDao.getAccessToken(),
                        tasksDao
                ),
                new RestCallback<Integer>() {
                    @Override
                    public void onResponse(Integer nextPage) {
                        scrollListener.resetLoadingFlag();
                        Log.d(TAG, "onResponse: next page = " + nextPage);
                        tasksAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFail(int code, String message) {
                    }
                });
    }
}
